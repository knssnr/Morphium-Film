(function () {

  // Create the ViewPort detector
  var viewDetector = document.createElement('div');
  document.getElementsByTagName('body')[0].insertBefore(viewDetector).id = 'viewport-detector';

  // Load and Resize events
  window.onresize = dynamicResizer;
  window.onload = dynamicResizer;

  function dynamicResizer() {
    var docWidth = window.innerWidth,
      docHeight = window.innerHeight;
    spanDimensions.innerHTML = docWidth + " x " + docHeight;
  }

  // Create <span class="dimensions"> and append
  var spanDimensions = document.createElement('span');
  spanDimensions.className = 'dimensions';
  document.getElementById('viewport-detector').appendChild(spanDimensions);

  // Create <span class="retina"> and append
  var spanRetina = document.createElement('span');
  spanRetina.className = 'retina';
  document.getElementById('viewport-detector').appendChild(spanRetina);

  // Create <span class="pixel-ratio"> and append
  var spanPixels = document.createElement('span');
  spanPixels.className = 'pixel-ratio';
  document.getElementById('viewport-detector').appendChild(spanPixels);
  spanPixels.innerHTML = 'Pixel Ratio: ' + window.devicePixelRatio;

  // Retina detect
  if (window.devicePixelRatio >= 2) {
    spanRetina.innerHTML = 'Retina Device';
  } else {
    spanRetina.innerHTML = 'No Retina Device';
  }
})();