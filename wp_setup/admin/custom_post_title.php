<?php
/********************************************************************************
Custom Post-type-titles
********************************************************************************/
function custom_post_type_titles($post_id) {
	$post_orig = get_post($post_id);

	switch ($post_orig->post_type) {


	case 'team':
		$post_updates = [];

		$post_updates['ID'] = $post_id;
	
		$post_updates['post_title'] = get_field('first_name', $post_id) . ' ' . get_field('last_name', $post_id);
		$post_updates['post_name'] = sanitize_title($post_updates['post_title']);
		

		remove_action('save_post', 'custom_post_type_titles', 100);
		wp_update_post($post_updates);
		add_action('save_post', 'custom_post_type_titles', 100);
		break;


	default:
		break;
	}
}
add_action('save_post', 'custom_post_type_titles', 100);

?>