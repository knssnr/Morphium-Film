<!-- <section class="main-panel front_tight light_scheme  text_<?php the_sub_field('direction'); ?> jumbotron p-md-5 d-flex   align-items-center" >
 -->
<div  class="section_content text_<?php echo $section_align;?>  <?php echo $section_style; ?>_scheme">
    <div class=" row inner_section justify-content-center align-self-center container">
        <h2 class="jumbotron-heading">
            <?php the_sub_field('titel');?>
        </h2>
        <p class="lead">
            <?php the_sub_field('content'); ?>
        </p>
    </div>
</div>

<!-- <div class="space"></div> -->
