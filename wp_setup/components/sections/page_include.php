<?php $post = get_sub_field('page'); ?>
<?php $more = get_sub_field('more');
$intersection = get_sub_field('intersection');

if ($post) : ?>
<?php if($more) $child_classes = $child_classes ." content_side_loader" ?>
<?php if( $intersection ) $child_classes = $child_classes ." intersection " ?>
<?php if(!$more) $child_classes = $child_classes ." single_content" ?>
<!--  side_right -->
<?php $postId = $post->ID;
$content_post = get_post($postId);
$content = $content_post->post_content;
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]&gt;', $content);


       // setup_postdata($post);  $post_slug = $post->post_name; ?>
 <?php if($intersection){?>
        <h3 class="inline_title container p-auto pl-5"><?php the_title(); ?></h3>
  <?php }
  if($more){ ?>
    <nav class="arrow_nav nav-slide page_side_loader load_full_page in">
        <!--page_reveal --> <a class="next " data-section="<?php echo $section_id; ?>" href="<?php echo get_permalink( $postId ); ?>" target="_blank" rel="noopener noreferrer">
            <span class="icon-wrap">
                <svg class="icon" width="32" height="32" viewBox="0 0 64 64"><use xlink:href="#arrow-right-1"></svg>
            </span>
            <div><span class="text">Erfahren Sie mehr..</span></div>
        </a>
      </nav>
      <nav class="arrow_nav nav-slide page_side_loader load_full_page out">
        <a class="next page_reveal" data-section="<?php echo $section_id; ?>" href="<?php echo get_permalink( $postId ); ?>" target="_blank" rel="noopener noreferrer">
            <span class="icon-wrap">
                <svg class="icon" width="32" height="32" viewBox="0 0 64 64"><use xlink:href="#arrow-right-1"></svg>
            </span>
           <div><span class="text">Erfahren Sie mehr..</span></div>
        </a>
      </nav>


<?php  }  ?>


  <div class="section_content container content_preview <?php echo $child_classes; ?>">
    <div class="p-md-5">
          <?php the_field('preview_text', $postId); ?>
    </div>
  </div>

<div class="section_content container content_await <?php echo $child_classes; ?>">
 <!--  <nav class="arrow_nav nav-slide page_side_loader">
    <a class="next page_reveal" data-section="<?php echo $section_id; ?>" href="#ueber_uns">
      <span class="icon-wrap">
        <svg class="icon" width="32" height="32" viewBox="0 0 64 64">
          <use xlink:href="#arrow-left-1">
        </svg>
      </span>
      <div>
        <span class="text">Seite schließen</span>
      </div>
    </a>
  </nav>
 -->

  <div class="container p-md-5">
    <?php echo $content; ?>
  </div>
</div>


<?php  wp_reset_postdata(); ?>
<?php endif; ?>
