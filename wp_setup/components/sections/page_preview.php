 <?php 
$align  = get_sub_field('align');

$page   = get_sub_field('page');
if( $page ): 
    $post = $page;
    setup_postdata( $post ); 
    $post_slug=$post->post_name;
    

     ?>


        <section class="front_tight light_scheme  text_<?php echo $align ?> align-items-center d-flex jumbotron" id="<?php echo $post_slug; ?>">
            <article class="inner_section justify-content-center align-self-center">
                <header>
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                </header>

                <div>
                    <p><?php the_field('preview_text'); ?></p>
                </div>

                <footer>
                    <a href="#" class="btn btn-primary">Mail</a>
                    <a href="<?php the_permalink(); ?>" class="btn btn-secondary">Mehr ...</a>
                </footer>
            </article>
        </section>
        

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>
