<div class="modal fade modal-effect" id="mainModal" tabindex="-1" role="dialog" aria-labelledby="mainModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered jumbotron" role="document">
    <div class="modal-content">
      <button type="button " class="close close_symb" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>


      <div class="modal-header">
        <div class="headlines">
          <h3 class="modal-title" id="mainModalLabel">Projekt</h3>
          <h4 class="modal-sub-title"><span id="subline">Subline</span></h4>
          <h4 class="modal-sub-title"><span id="kunde">Kunde</span><small id="produktio">Produktion</small></h4>
        </div>
        <div class="header_side_images ">
          <img class="header_image" />
        </div>

      </div>

      <div class="modal-body">
        <div id="modal_Media_Video" class="embed-responsive embed-responsive-16by9 col-md-12 col-lg-9">
          <!--   <video frameborder="0" playsinline autoplay muted loop preload="metadata" height="100%" width="100%" id="modal_video">
              <source src="" type="video/mp4">
            </video> -->
          <iframe class="youtube_embed" id="modal_iframe" allowfullscreen height="100%" width="100%" frameborder="0"
            scrolling="no"></iframe>

        </div>

        <div class="modal_info col-md-12 col-lg-3">

          <div class="generall_info ">
             <figure data-tilt  class="kunde open_modal visible">
                  <img id="project_client_logo" class="kunden_logo" src="" alt=""/>
             </figure>
             <h4 id="project_client"></h4>
             <p id="content_description"></p>
          </div>

          <div class="modal-footer">
            <div class="social_share ">
                <a target="_blank" rel="noopener noreferrer"  class="twitter_share_link" href="http://twitter.com/home?status=<?php echo urlencode("Currently reading: "); ?><?php the_permalink(); ?>" title="Share this article with your Twitter followers">Tweet
                this!</a>
                <a target="_blank" rel="noopener noreferrer" class="fb_share_link" href=""  title="Share this post on Facebook">Share on Facebook</a>
            </div>

            <button type="button" class="btn btn-primary">Share</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>


  </div>

  <div class="modal_background"></div>
</div>
