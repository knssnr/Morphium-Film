<?php
/* Sets the path to the core framework directory. */
if ( !defined( 'HYBRID_DIR' ) )
    define( 'HYBRID_DIR', trailingslashit( TEMPLATEPATH ) . '/core/hybrid/' );

/* Sets the path to the core framework directory URI. */
if ( !defined( 'HYBRID_URI' ) )
    define( 'HYBRID_URI', trailingslashit( TEMPLATEPATH ) . '/core/hybrid/hybrid.php' );
/* Load the core theme framework. */
require_once( trailingslashit( TEMPLATEPATH ) . '/core/hybrid/hybrid.php' );
//$theme = new Hybrid();

/**
 * Theme setup function.  This function adds support for theme features and defines the default theme
 * actions and filters.
 *
 * @since 0.1.0
 */



add_action( 'after_setup_theme', 'complex_theme_setup' );
function complex_theme_setup() {
    add_theme_support( 'hybrid-core-widgets' );
    add_theme_support( 'hybrid-core-template-hierarchy' );
    add_theme_support( 'loop-pagination' );
    add_theme_support( 'get-the-image' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'cleaner-caption' );
    add_theme_support( 'cleaner-gallery' );
    add_theme_support( 'hybrid-core-theme-settings' );
}


add_action( 'after_setup_theme', 'complex_additional_setup' );
function complex_additional_setup() {
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/admin/admin_settings.php' );
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/sidebars/sidebar_settings.php' );
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/menus/menu_settings.php' );
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/admin/custom_post-types.php');
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/admin/style.php');
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/admin/admin_pages.php' );
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/admin/widgets.php' );
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/admin/featured_image.php');
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/admin/custom_field.php');
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/components/ajax_query.php' );
        require_once(trailingslashit(TEMPLATEPATH) . 'wp_setup/admin/acfs.php');

       // if (strpos($_SERVER['SERVER_NAME'], '.de') === true) {}

    }

add_action('wp_enqueue_scripts', 'complex_add_scripts');
function complex_add_scripts() {

    wp_enqueue_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array('jquery'),'0.1.0' , true);

    //wp_enqueue_script('jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js', array('jquery'));
    wp_enqueue_script('tilt', 'https://cdnjs.cloudflare.com/ajax/libs/tilt.js/1.2.1/tilt.jquery.min.js', array('jquery'));
    wp_enqueue_script('slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array('jquery'));



    wp_enqueue_script('vendors', get_template_directory_uri() . '/core/js/resource_loader.js', array('jquery','tilt'), '0.1.0', true);
    wp_register_script('script_loader', get_template_directory_uri() . '/core/js/script_loader.js', array('jquery', 'vendors'), '0.1.0', true);
    wp_enqueue_script('script_loader');


    wp_enqueue_script('ajax-pagination', get_template_directory_uri() . '/src/js/ajax_query_loader.js', array('script_loader', 'jquery','tilt'), '0.1.0', true);
    wp_localize_script(
        'ajax-pagination',
        'ajaxpagination',
        array(
            'ajaxurl' => admin_url('admin-ajax.php')
        )
    );
}
/* $to = 'sascha-Darius@kniessner.com';
$subject = 'The subject';
$body = 'The email body content';
$headers = array('Content-Type: text/html; charset=UTF-8');

wp_mail($to, $subject, $body, $headers); */

// add ie conditional html5 shim to header
function add_ie_html5_shim()
{
    echo '<!--[if lt IE 9]>';
    echo '<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>';
    echo '<![endif]-->';
}
add_action('wp_head', 'add_ie_html5_shim');

function sonderzeichen($string){
    $string = strtolower($string);
    $search = array("Ü","ä", "ö", "ü", "ä", "ö", "ü", "ß", "´"," ");
    $replace = array("ue","ae", "oe", "ue", "ae", "oe", "ue", "ss","-","-");
    return str_replace($search, $replace, $string);
}



require_once 'core/Mobile_Detect.php';

/* check if user using smaller mobile device */
  function my_wp_is_mobile() {
    $detect = new Mobile_Detect;
      if( $detect->isMobile() && !$detect->isTablet() ) {
        return true;
      } else {
        return false;
      }
  }
  
  /* check if user using tablet device */
  function my_wp_is_tablet() {
    $detect = new Mobile_Detect;
    if( $detect->isTablet() ) {
      return true;
    } else {
      return false;
    }
}

?>